﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ThapovanHandsOnProject.ServiceInterface;
using ThapovanHandsOnProject.Services;

namespace ThapovanHandsOnProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // GET: api/User
        [HttpGet]
        public async Task<IEnumerable<Users>> GetUsers()
        {
            return await _userService.GetUsers();
        }


        // GET: api/User/jaga89@gmail.com
        [HttpGet("{email}")]
        public async Task<ActionResult<Users>> GetUser(string email)
        {
            var user = await _userService.GetUser(email);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }


        // POST: api/User
        [HttpPost]
        public async Task<IActionResult> PostUser(Users user)
        {
            try
            {
                var result = await _userService.PostUser(user);
                if (result != null)
                {
                    return Created("Registration successful", user);
                }
                else return BadRequest("Registration Failed");
            }
            catch (Exception e)
            {
                if(e.Message== "You have already registered. Please login")
                {
                    return Ok(e.Message);
                }
                else return BadRequest("Registration failed please check the all the credentials are correct.");
            }
        }


    }
}
