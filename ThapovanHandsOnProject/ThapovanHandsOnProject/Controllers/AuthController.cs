﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ThapovanHandsOnProject.Models;
using ThapovanHandsOnProject.ServiceInterface;

namespace ThapovanHandsOnProject.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;

        public AuthController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost,Route("login")]
        public async Task<IActionResult> Login([FromBody] Login user)
        {
            if (user == null)
            {
                return BadRequest("Invalid client request");
            }
            var result =  _userService.GetUserDetails(user.Username,user.Password);
            if (result != null)
            {
                if (user.Username == result.Email && user.Password == result.Password)
                {
                    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                    var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                    var tokenOptions = new JwtSecurityToken(
                        issuer: "https://localhost:5001",
                        audience: "https://localhost:5001",
                        claims: new List<Claim>(),
                        expires: DateTime.Now.AddMinutes(5),
                        signingCredentials: signingCredentials
                    );
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                    if (result.RoleId == 2)
                    {
                        return Ok(new { Token = tokenString, Role="Manager" });
                    }
                    else return Ok(new { Token = tokenString, Role = "User" });
                }
                return Unauthorized();
            }
            else
            {
                return BadRequest("Please provide valid credentials OR Register.");
            }
        }
    }
}