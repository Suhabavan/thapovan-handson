﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ThapovanHandsOnProject.ServiceInterface;

namespace ThapovanHandsOnProject.Services
{
    
    public class UserService:IUserService
    {
        private readonly ThapovanUserRegistrationDBContext _context;
        public UserService(ThapovanUserRegistrationDBContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Users>> GetUsers()
        {
            var users = await _context.Users.Where(u => u.RoleId == 1).ToListAsync<Users>();
            return users;
        }

        
        public async Task<Users> GetUser(string email)
        {
            Users user = await _context.Users.FindAsync(email);

            if (user == null)
            {
                return null;
            }

            return user;
        }

        public Users GetUserDetails(string email,string password)
        {
            var user =  _context.Users.Where(u => u.Email == email && u.Password == password).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            return user;
        }


        public async Task<Users> PostUser(Users user)
        {
            try
            {
                user.RoleId = 1;
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                var sqlException = ex.InnerException as SqlException;
                var primaryKeyErrorNumber = sqlException.Number; //Primary Key Validation
                if(primaryKeyErrorNumber== 2627)
                {
                    throw new Exception("You have already registered. Please login");
                }
                throw new Exception(ex.Message); 
            }
            return await GetUser(user.Email);
        }



    }
}
