﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThapovanHandsOnProject
{
    public partial class Users
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Gender { get; set; }
        [Key]
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string AreaOfInterest { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
    }
}
