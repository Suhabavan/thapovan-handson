﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ThapovanHandsOnProject.ServiceInterface
{
    public interface IUserService
    {
        Task<IEnumerable<Users>> GetUsers();

        Task<Users> GetUser(string email);

        Task<Users> PostUser(Users user);

        Users GetUserDetails(string email, string password);

    }
}
